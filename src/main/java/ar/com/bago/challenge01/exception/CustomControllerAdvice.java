package ar.com.bago.challenge01.exception;

import ar.com.bago.challenge01.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomControllerAdvice {

    @ExceptionHandler(value = {NotFoundException.class})
    public ResponseEntity<ErrorResponse> resourceNotFound(NotFoundException ex) {
        return new ResponseEntity<>(new ErrorResponse(4001, ex.getMessage()), HttpStatus.NOT_FOUND);
    }
}
