# Challenge

Los endpoints disponibles son los siguientes:

http://localhost:9090/basic 
Devuelve un listado de objetos.

http://localhost:9090/basic/1
Devuelve el primer elemento con el Id = 1

Pero, si se ingresa un id inexistente, el response debe ser

{"status":4001,"message":"Id 2 no encontrado"}

con status 404.

## Realizar lo siguiente:
+ Manejar las expeciones de forma centralizada. 
+ Refactorizar y simplicar el proyecto.

# Solución implementada

Dado que se pretende que la solución escale de manera organica, se plantea que el manejo de excepciones, ya sea manejadas o no manejadas se centralice y que se devuelva un único modelo de respuesta, y que la información que contenga dependa del tipo de excepción.<br>
Para ello y dado que estamos trabajando con Spring podemos una clase con la annotation @ControllerAdvice.<br>
Este mecanismo es extremadamente flexible simple y muy flexible:
* Nos da control total sobre el cuerpo de la respuesta, así como el código de estado HTTP.
* Proporciona mapeo de varias excepciones al mismo método, para ser manejadas juntas.
* Hace un buen uso de la respuesta RESTful ResposeEntity más nueva.<br>

# Implementación
La solución fue implementada en la clase CustomControllerAdvice dentro del package java.ar.com.bago.challenge01.exception <br>
Como mejora a esta solución y para que como se dijo anteriormente la aplicación pueda evolucionar organicamente se proponen las siguientes refactorizaciones:
1. Extraer la clase que tiene la responsabilidad del controller advice a una clase dentro de un nuevo package llamado: java.ar.com.bago.challenge01.framework.advices
2. Mover la clase NotFoundException a un nuevo package java.ar.com.bago.challenge01.framework.exceptions
3. Mover todos los el resto de los componentes a un package llamado java.ar.com.bago.challenge01.rest
4. Separar los modelos de negocio, de las respuestas de la capa de presentación y de las entidades de dominio.